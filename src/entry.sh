#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

PROJECT_NAME=$1
STANDARD_README='https://gitlab.com/olmesm/standard-readme/raw/master/README.md'
QUICK_PROJECT_URL='https://gitlab.com/olmesm/quick-prj/raw/master'
NVM_DIR="$HOME/.nvm"

get_file () {
  wget "$1" 2>/dev/null || curl -O "$1"
}


# CHECK PROJECT NAME SUPPLIED
if [ -z "$PROJECT_NAME" ]
  then
    echo "No project name supplied"
fi

# MAKE A DIRECTORY IN THE CURRENT PATH
echo "Making directory $PROJECT_NAME"
mkdir "$PROJECT_NAME"
cd "$PROJECT_NAME"

# GIT INIT
git init

# NVM INSTALL --LTS
source ~/.nvm/nvm.sh install --lts

# ADD .NVMRC FILE
node -v > .nvmrc

# NPM INIT -Y
npm init -y

# ADD .GITIGNORE
get_file "$QUICK_PROJECT_URL/.gitignore"

# ADD .EDITORCONFIG
get_file "$QUICK_PROJECT_URL/.editorconfig"

# INSTALL NPM PACKAGE STANDARD
npm i standard -D

# ADD AS A RECOMMENDED EXTENSION
mkdir .vscode
get_file "$QUICK_PROJECT_URL/.vscode/extensions.json"
mv extensions.json .vscode/

# COPY DOWN A BASIC README.MD FILE
get_file "$STANDARD_README"

# BASIC PROJECT STRUCTURE
mkdir -p src config
echo "console.log('hello')" > src/index.js
echo "{ \"API_URL\": \"http://dev.my-api.com\" }" > config/template.environment.json

# READY
echo "

>>>> Ready for the first commit!

$ cd $PROJECT_NAME

"

} # this ensures the entire script is downloaded #
