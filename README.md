# Quick Project

Setup a quick node project with good structure and standard.

```sh
curl -o- https://gitlab.com/olmesm/quick-prj/raw/master/src/entry.sh | bash -s <project-name>

# Or with wget
wget -qO- https://gitlab.com/olmesm/quick-prj/raw/master/src/entry.sh | bash -s <project-name>
```

This will:

1. Make a directory in the current path
1. git init
1. [nvm] install --lts
1. Add [.nvmrc] file
1. npm init -y
1. Add [.gitignore]
1. Add [.editorconfig]
1. Install npm package [standard]
1. Copy down a basic [README.md] file
1. Basic project structure

## Add to shell

Add to your `.bashrc` or `.zshrc` file

```sh
# quickprj

newprj () {
  curl -o- https://gitlab.com/olmesm/quick-prj/raw/master/src/entry.sh | bash -s "$1"
}
```

Then

```sh
newprj <project-name>
```

<!-- Markdown References -->

[.editorconfig]: https://editorconfig.org/
[.gitignore]: https://git-scm.com/docs/gitignore
[.nvmrc]: https://github.com/creationix/nvm#nvmrc
[nvm]: https://github.com/creationix/nvm
[standard]: https://www.npmjs.com/package/standard
[README.md]: https://gitlab.com/olmesm/standard-readme
